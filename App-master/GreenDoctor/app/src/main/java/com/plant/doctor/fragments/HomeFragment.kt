package com.plant.doctor.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.provider.MediaStore
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.plant.doctor.R
import com.plant.doctor.activities.ExtrasActivity
import com.plant.doctor.activities.MainActivity
import com.plant.doctor.helpers.Classifier
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.io.IOException


class HomeFragment : Fragment() {
    private lateinit var mClassifier: Classifier
    private lateinit var mBitmap: Bitmap

    private val mCameraRequestCode = 0
    private val mGalleryRequestCode = 2

    private val mInputSize = 224
    private val mModelPath = "plant_disease_model.tflite"
    private val mLabelPath = "plant_labels.txt"
    private val mSamplePath = "maize.jpeg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.home)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        mClassifier =
            Classifier(context!!.assets, mModelPath, mLabelPath, mInputSize)

        resources.assets.open(mSamplePath).use {
            mBitmap = BitmapFactory.decodeStream(it)
            mBitmap = Bitmap.createScaledBitmap(mBitmap, mInputSize, mInputSize, true)
            rootView.mPhotoImageView.setImageBitmap(mBitmap)

            //set event listeners
            rootView.mCameraButton.setOnClickListener {
                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(callCameraIntent, mCameraRequestCode)
            }

            rootView.mGalleryButton.setOnClickListener {
                val callGalleryIntent = Intent(Intent.ACTION_PICK)
                callGalleryIntent.type = "image/*"
                startActivityForResult(callGalleryIntent, mGalleryRequestCode)
            }
            rootView.mDetectButton.setOnClickListener {
                val results = mClassifier.recognizeImage(mBitmap).firstOrNull()

                if (results?.title.equals("corn maize cercospora leaf spot gray leaf spot") ||
                    results?.title.equals("corn maize common rust") ||
                    results?.title.equals("corn maize northern leaf blight") ||
                    results?.title.equals("corn maize healthy")
                ) {
                    mResultTextView.text = results?.title + "\n Confidence:" + results?.confidence
                } else if (results?.title.equals("null")) {
                    mResultTextView.text = "Not a plant"
                } else {
                    mResultTextView.text = "Not a amaize plant"
                }


            }

            rootView.mLearnTextView.setOnClickListener {
                val intent = Intent(context, ExtrasActivity::class.java)
                startActivity(intent)
            }
            return rootView
        }


    }

    //after camera intent
    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == mCameraRequestCode) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                mBitmap = data.extras!!.get("data") as Bitmap
                mBitmap = scaleImage(mBitmap)
                val toast = Toast.makeText(
                    context,
                    ("Image crop to: w= ${mBitmap.width} h= ${mBitmap.height}"),
                    Toast.LENGTH_LONG
                )
                toast.setGravity(Gravity.BOTTOM, 0, 20)
                toast.show()
                mPhotoImageView.setImageBitmap(mBitmap)
                mResultTextView.text = "Your photo image set now."
            } else {
                Toast.makeText(context, "Camera cancelled..", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == mGalleryRequestCode) {
            if (data != null) {
                val uri = data.data

                try {
                    mBitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, uri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                println("Success!!!")
                mBitmap = scaleImage(mBitmap)
                mPhotoImageView.setImageBitmap(mBitmap)

            }
        } else {
            Toast.makeText(context, "Unrecognized request code", Toast.LENGTH_LONG).show()

        }

    }

    //scale camera image
    private fun scaleImage(bitmap: Bitmap): Bitmap {
        val orignalWidth = bitmap!!.width
        val originalHeight = bitmap.height
        val scaleWidth = mInputSize.toFloat() / orignalWidth
        val scaleHeight = mInputSize.toFloat() / originalHeight
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)
        return Bitmap.createBitmap(bitmap, 0, 0, orignalWidth, originalHeight, matrix, true)
    }

}
