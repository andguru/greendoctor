package com.plant.doctor.activities

import android.os.Bundle
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.plant.doctor.helpers.Classifier
import com.plant.doctor.R
import com.plant.doctor.fragments.CommunityFragment
import com.plant.doctor.fragments.HomeFragment
import com.plant.doctor.fragments.SettingsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var mClassifier: Classifier
    private lateinit var mBitmap: Bitmap

    private val mCameraRequestCode = 0
    private val mGalleryRequestCode = 2

    private val mInputSize = 224
    private val mModelPath = "plant_disease_model.tflite"
    private val mLabelPath = "plant_labels.txt"
    private val mSamplePath = "soybean.JPG"

    private var logoutbtn: Button?=null
    private var sytte: TextView? = null
    private var prevve: TextView? = null
    private var ttle3: TextView? = null


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //logoutbtn=findViewById<View> (R.id.logout_btn) as Button

        supportActionBar?.title = "Home"

        //init bottom navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if (savedInstanceState == null) {
            val fragment = HomeFragment()
            supportFragmentManager.beginTransaction().replace(
                R.id.container, fragment, fragment.javaClass.simpleName
            ).commit()
        }

      //  logoutbtn!!.setOnClickListener {
            //start main page
         //   val intent = Intent(this, LoginActivity::class.java)
        //    startActivity(intent)
      //  }

    }



    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_home -> {
                    val fragment = HomeFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.container, fragment, fragment.javaClass.simpleName
                    ).commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_community -> {
                    val fragment = CommunityFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.container, fragment, fragment.javaClass.simpleName
                    ).commit()
                    return@OnNavigationItemSelectedListener true

                }
                R.id.navigation_settings -> {
                    val fragment = SettingsFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.container, fragment, fragment.javaClass.simpleName
                    ).commit()
                    return@OnNavigationItemSelectedListener true

                }
            }
            false
        }



    fun logOut(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    fun goTosingle0(view: View) {
        sytte=findViewById<View> (R.id.sytt01) as TextView
        prevve=findViewById<View> (R.id.prevv01) as TextView
        ttle3=findViewById<View> (R.id.ttl3) as TextView
        val str: String = sytte!!.text.toString()
        val str2: String = prevve!!.text.toString()
        val str3: String = ttle3!!.text.toString()
        val intent = Intent(this, Single::class.java)
        intent.putExtra("Title", str3)
        intent.putExtra("Name", str)
        intent.putExtra("Email", str2)
        intent.putExtra("Phone", "R.drawable.downymildew")
        startActivity(intent)
    }

    fun goTosingle002(view: View) {
        sytte=findViewById<View> (R.id.sytt002) as TextView
        prevve=findViewById<View> (R.id.prevv002) as TextView
        ttle3=findViewById<View> (R.id.ttl002) as TextView
        val str: String = sytte!!.text.toString()
        val str2: String = prevve!!.text.toString()
        val str3: String = ttle3!!.text.toString()
        val intent = Intent(this, Single::class.java)
        intent.putExtra("Title", str3)
        intent.putExtra("Name", str)
        intent.putExtra("Email", str2)
        intent.putExtra("Phone", "R.drawable.downymildew")
        startActivity(intent)
    }

    fun goTosingle(view: View) {
        sytte=findViewById<View> (R.id.sytt01) as TextView
        prevve=findViewById<View> (R.id.prevv01) as TextView
        ttle3=findViewById<View> (R.id.ttl3) as TextView
        val str: String = sytte!!.text.toString()
        val str2: String = prevve!!.text.toString()
        val str3: String = ttle3!!.text.toString()
        val intent = Intent(this, Single::class.java)
        intent.putExtra("Title", str3)
        intent.putExtra("Name", str)
        intent.putExtra("Email", str2)
        intent.putExtra("Phone", "R.drawable.downymildew")
        startActivity(intent)
    }
    fun goTosingle2(view: View) {
        sytte=findViewById<View> (R.id.sytt01) as TextView
        prevve=findViewById<View> (R.id.prevv01) as TextView
        ttle3=findViewById<View> (R.id.ttl0) as TextView
        val str: String = sytte!!.text.toString()
        val str2: String = prevve!!.text.toString()
        val str3: String = ttle3!!.text.toString()
        val intent = Intent(this, Single::class.java)
        intent.putExtra("Title", str3)
        intent.putExtra("Name", str)
        intent.putExtra("Email", str2)
        intent.putExtra("Phone", "R.drawable.downymildew")
        startActivity(intent)
    }


}

