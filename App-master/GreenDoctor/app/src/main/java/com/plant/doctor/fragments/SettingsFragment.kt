package com.plant.doctor.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.plant.doctor.R
import com.plant.doctor.activities.LoginActivity
import com.plant.doctor.activities.MainActivity


class SettingsFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.settings)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val logoutbtn : Button?=view?.findViewById(R.id.logout_btn)

         // logoutbtn?.setOnClickListener { v: View -> buttonClicked(v)}

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    private fun buttonClicked(view: View) {
        val intent = Intent (activity, LoginActivity::class.java)
        startActivity(intent)
    }

     fun goTosingle(view: View) {
        var intent=Intent(activity,MainActivity::class.java)
        startActivity(intent)
    }


}

