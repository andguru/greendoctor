package com.plant.doctor.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.plant.doctor.R

class ExtrasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extras)
    }
}
