package com.plant.doctor.activities

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.plant.doctor.R

class CreateAccountActivity : AppCompatActivity() {

    private val TAG = "CreateAccountActivity"

    //UI elements
    private var login_text: TextView? = null
    private var etFirstName: EditText? = null
    private var etLastName: EditText? = null
    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    private var btnCreateAccount: Button? = null
    private var mProgressBar: ProgressDialog? = null
    private var mAnonymus:Button?=null

    //firebase references
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        initViews()
    }

    private fun initViews() {
        login_text=findViewById<View> (R.id.login_text_1) as TextView
        etFirstName = findViewById<View>(R.id.et_first_name) as EditText
        etLastName = findViewById<View>(R.id.et_last_name) as EditText
        etEmail = findViewById<View>(R.id.et_email) as EditText
        etPassword = findViewById<View>(R.id.et_password) as EditText
        btnCreateAccount = findViewById<View>(R.id.btn_login) as Button
        mProgressBar = ProgressDialog(this)
        mAnonymus=findViewById<View>(R.id.btn_anonymus) as Button

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()

        btnCreateAccount!!.setOnClickListener { createNewAccount() }
        login_text!!.setOnClickListener {
            //start main page
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        mAnonymus!!.setOnClickListener{proceedAnonymus()}
    }

    private fun proceedAnonymus() {
        var intent=Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    private fun createNewAccount() {
        //var firstName = etFirstName?.text.toString()
        var lastName = etLastName?.text.toString()
        var email = etEmail?.text.toString()
        var password = etPassword?.text.toString()

        //validation
        if (!TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(
                password
            ) && !TextUtils.isEmpty(email)
        ) {

            mProgressBar!!.setMessage("Registering User...")
            mProgressBar!!.show()

            mAuth!!
                .createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    mProgressBar!!.hide()

                    if (task.isSuccessful) {
                        //Sign in success, update UI with the signed-in user's info
                        Log.d(TAG, "createUserWithEmail:success")

                        //start main page
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        Log.w(TAG, task.exception)
                        Toast.makeText(
                            this@CreateAccountActivity,
                            task.exception?.message,
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
        } else {
            Toast.makeText(this, "Enter all details", Toast.LENGTH_SHORT).show()
        }
    }
}
